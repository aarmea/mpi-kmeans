#ifndef KMEANS2_H

#include <array>
#include <cassert>
#include <cstddef>
#include <limits>
#include <type_traits>
#include <utility>
#include <vector>

#include <mpi.h>

// Print timing in CPU cycles (newline separated) to stdout
// Enable with -DTIMING
#ifdef TIMING
  #include <iostream>
  #include "rdtsc.h"
#endif

// Add two vectors, storing the result in the first vector.
template< class T, std::size_t N >
void add_in(std::array< T, N > &a, const std::array< T, N > &b) {
  for (std::size_t i = 0; i < N; ++i)
    a[i] += b[i];
}

// Divide a vector by a scalar in place.
template< class T, std::size_t N >
void div_in(std::array< T, N > &a, const T &b) {
  for (std::size_t i = 0; i < N; ++i)
    a[i] /= b;
}

// Return the distance squared between two points.
template< class DistT, class T, std::size_t N >
DistT length_squared(const std::array< T, N > &a,
  const std::array< T, N > &b) {
  DistT acc = 0;
  for (std::size_t i = 0; i < N; ++i)
    acc += (b[i] - a[i]) * (b[i] - a[i]);
  return acc;
}

// Cluster the sample data.
template< class DistT, class T, std::size_t N >
void kmeans(const int rank, const int num_ranks,
  const std::vector< std::array< T, N > > &samples,
  std::vector< std::array< T, N > > &centers) {
  static const std::array< T, N > zero = {};

  std::vector< std::size_t > assignments(samples.size());

  #ifdef TIMING
    std::vector< unsigned long long > loop_times;
    std::size_t data_size = sizeof(int) +
      sizeof(std::array< T, N >) * centers.size() +
      sizeof(unsigned long) * centers.size();
    unsigned long long start_time = rdtsc();
  #endif

  while (true) {
    std::vector< std::array< T, N > > new_centers(centers.size(), zero);
    std::vector< unsigned long > center_element_count(centers.size());

    bool assignments_changed = false;

    #ifdef TIMING
      unsigned long long loop_start_time = rdtsc();
    #endif

    // Assign all samples to the nearest cluster.
    for (std::size_t i = samples.size() * rank / num_ranks; i < samples.size() * (rank + 1) / num_ranks; ++i) {
      DistT min_dist = std::numeric_limits< DistT >::max();
      std::size_t min_center = 0;

      for (std::size_t j = 0; j < centers.size(); ++j) {
        const DistT dist = length_squared< DistT >(centers[j], samples[i]);
        if (dist < min_dist) {
          min_dist = dist;
          min_center = j;
        }
      }

      if (assignments[i] != min_center) {
        assignments_changed = true;
        assignments[i] = min_center;
      }

      ++center_element_count[min_center];
      add_in(new_centers[min_center], samples[i]);
    }

    // Terminate if no assignments have changed globally.
    MPI_Allreduce(MPI_IN_PLACE, &assignments_changed, 1, MPI_INT, MPI_LOR,
      MPI_COMM_WORLD);
    if (!assignments_changed)
      break;

    // Recalculate cluster centers.
    MPI_Allreduce(&new_centers[0], &centers[0], centers.size() * N, MPI_FLOAT,
      MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(MPI_IN_PLACE, &center_element_count[0], centers.size(),
      MPI_UNSIGNED_LONG, MPI_SUM, MPI_COMM_WORLD);

    for (std::size_t i = 0; i < centers.size(); ++i) {
      if (center_element_count[i])
        div_in(centers[i], T(center_element_count[i]));
    }

    #ifdef TIMING
      loop_times.push_back(rdtsc()  - loop_start_time);
    #endif
  }

  #ifdef TIMING
    if (rank == 0) {
      std::cout << "Rank 0 total time: " << rdtsc()-start_time << "\n";
      std::cout << "Iteration\tTime:\n";
      for (std::size_t i = 0; i < loop_times.size(); ++i) {
        std::cout << i << "\t" << loop_times[i] << "\t"
          << data_size << "\t" << data_size << "\n";
      }
      std::cout << "\n";
    }
  #endif
}

#endif
