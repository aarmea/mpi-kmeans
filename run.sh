#!/bin/sh

coresPerNode=16

for nodes in 64 32 16 8 4 2 1
do
  srun -o bgq-%j-main-$nodes --nodes=$nodes \
    --ntasks=$((nodes*coresPerNode)) --overcommit \
    ./main
done

