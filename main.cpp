#include <array>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <unordered_set>
#include <utility>
#include <vector>

#include <mpi.h>

#include "kmeans.h"
#include "cmtrand.h"

// XXX mpic++ -std=c++0x -o main main.cpp
// XXX mpirun -np 8 ./main

typedef std::array< float, 4 > sample_type;

int main(int argc, char *argv[]) {
  int rank;
  int num_ranks;

  // Initialize MPI.
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  const std::size_t num_centers = 1024;

  if (num_centers < num_ranks) {
    if (rank == 0) {
      std::cerr << "Fewer centers (" << num_centers << ") than ranks (" <<
        num_ranks << ")\n";
    }
    MPI_Finalize();
    return 1;
  }

  // Initialize per-rank buffers.
  std::vector< int > center_ranks;
  std::vector< sample_type > centers;
  std::vector< sample_type > samples;

  // Generate random sample data.
  unsigned int seed = 23456;
  init_genrand(seed);

  sample_type sample;
  for (std::size_t i = 0; i < 1000000; ++i) {
    sample[0] = genrand_real1();
    sample[1] = genrand_real1();
    sample[2] = genrand_real1();
    sample[3] = genrand_real1();
    samples.push_back(sample);
  }

  // Pick centers randomly from the data set.
  std::unordered_set< std::size_t > picked_centers;
  for (std::size_t i = 0; i < num_centers; ++i) {
    std::size_t center_index;
    // Skip duplicates
    do {
      center_index = genrand_int32() % samples.size();
    } while (picked_centers.count(center_index));

    picked_centers.insert(center_index);
    center_ranks.push_back(i % num_ranks);
    centers.push_back(samples[i]);
  }

  // Run k-means.
  kmeans< double >(rank, num_ranks, samples, center_ranks, centers);

  if (rank == 0) {
    for (std::size_t i = 0; i < num_centers; ++i) {
      std::cout << "centers[" << i << "]: { " << centers[i][0] << ", "
        << centers[i][1] << " }\n";
    }
  }

  MPI_Finalize();
  return 0;
}

