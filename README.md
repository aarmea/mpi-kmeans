Massively Parallel k-Means Clustering
=====================================

Authors
-------

Anthony DeRossi, derosa3@rpi.edu

Albert Armea, armeaa@rpi.edu

Abstract
--------

k-means clustering is an algorithm for identifying cluster centers in data
sets. It has a number of applications in machine learning as an unsupervised
learning technique, and more intuitively in computational geometry. In this
project, we design and implement two methods that perform k-means clustering
using MPI. We test these methods on a Blue Gene/Q supercomputer using various
numbers of nodes to evaluate the efficiency and scalability of these methods.
The first method has some interesting performance characteristics, improving
in speed and bandwidth usage with the number of iterations, but the second
method provides superior performance and bandwidth usage for all cases.

The full paper is located in `project.pdf`.

Compiling
---------

Compile and run method 1 with:

    mpic++ -std=c++0x -O3 -DTIMING main.cpp -o main
    mpirun -np PROCESSES ./main

Compile and run method 2 with:

    mpic++ -std=c++0x -O3 -DTIMING main2.cpp -o main2
    mpirun -np PROCESSES ./main2

On the Blue Gene/Q, compile both with

    module load gnu
    mpic++ -std=c++0x -O3 -DTIMING main.cpp -o main
    mpic++ -std=c++0x -O3 -DTIMING main2.cpp -o main2

and run with

    sbatch --time 60 --nodes 64 --partition small ./run.sh
    sbatch --time 60 --nodes 64 --partition small ./run2.sh

