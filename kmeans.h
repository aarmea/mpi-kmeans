#ifndef KMEANS_H

#include <array>
#include <cassert>
#include <cstddef>
#include <limits>
#include <type_traits>
#include <utility>
#include <vector>

#include <mpi.h>

// Print timing in CPU cycles (newline separated) to stdout
// Enable with -DTIMING
#ifdef TIMING
  #include <iostream>
  #include "rdtsc.h"
#endif

// Add two vectors, storing the result in the first vector.
template< class T, std::size_t N >
void add_in(std::array< T, N > &a, const std::array< T, N > &b) {
  for (std::size_t i = 0; i < N; ++i)
    a[i] += b[i];
}

// Divide a vector by a scalar in place.
template< class T, std::size_t N >
void div_in(std::array< T, N > &a, const T &b) {
  for (std::size_t i = 0; i < N; ++i)
    a[i] /= b;
}

// Return the distance squared between two points.
template< class DistT, class T, std::size_t N >
DistT length_squared(const std::array< T, N > &a,
  const std::array< T, N > &b) {
  DistT acc = 0;
  for (std::size_t i = 0; i < N; ++i)
    acc += (b[i] - a[i]) * (b[i] - a[i]);
  return acc;
}

// Assign all samples to the nearest center, adding them to the sample and
// assignment buffers according to their rank and returning whether any
// assignments have changed.
template< class DistT, class T, std::size_t N >
bool kmeans_assignment(const int rank,
  const std::vector< int > &center_ranks,
  const std::vector< std::array< T, N > > &centers,
  const std::vector< std::array< T, N > > &samples,
  std::vector< std::size_t > &assignments,
  std::vector< std::vector< unsigned long > > &send_ownership) {
  assert(centers.size() == center_ranks.size());
  assert(samples.size() == assignments.size());

  bool assignments_changed = false;

  // Assign each sample belonging to this rank to the nearest center, changing
  // its ownership to another rank if necessary.
  for (std::size_t i = 0; i < samples.size(); ++i) {
    if (center_ranks[assignments[i]] != rank)
      continue;

    DistT min_dist = std::numeric_limits< DistT >::max();
    std::size_t min_center = 0;

    for (std::size_t j = 0; j < centers.size(); ++j) {
      const DistT dist = length_squared< DistT >(centers[j], samples[i]);
      if (dist < min_dist) {
        min_dist = dist;
        min_center = j;
      }
    }

    if (assignments[i] != min_center) {
      assignments_changed = true;
      assignments[i] = min_center;

      const int center_rank = center_ranks[min_center];
      if (center_rank != rank) {
        // This sample belongs to another rank.
        send_ownership[center_rank].push_back(i);
        send_ownership[center_rank].push_back(min_center);
      }
    }
  }

  return assignments_changed;
}

// Calculate new centers for each cluster associated with a rank.
template< class T, std::size_t N >
void kmeans_centers(const int rank,
  const std::vector< int > &center_ranks,
  std::vector< std::array< T, N > > &centers,
  std::vector< std::size_t > &center_element_count,
  const std::vector< std::array< T, N > > &samples,
  const std::vector< std::size_t > &assignments) {
  static const std::array< T, N > zero = {};

  assert(centers.size() == center_ranks.size());

  for (std::size_t i = 0; i < centers.size(); ++i) {
    if (center_ranks[i] == rank)
      centers[i] = zero;
  }

  center_element_count.assign(centers.size(), 0);

  for (std::size_t i = 0; i < samples.size(); ++i) {
    const std::size_t sample_center = assignments[i];
    if (center_ranks[sample_center] != rank)
      continue;

    add_in(centers[sample_center], samples[i]);
    ++center_element_count[sample_center];
  }

  for (std::size_t i = 0; i < centers.size(); ++i) {
    if (center_ranks[i] == rank && center_element_count[i])
      div_in(centers[i], T(center_element_count[i]));
  }
}

// Cluster the sample data.
template< class DistT, class T, std::size_t N >
void kmeans(const int rank, const int num_ranks,
  const std::vector< std::array< T, N > > &samples,
  const std::vector< int > &center_ranks,
  std::vector< std::array< T, N > > &centers) {
  assert(centers.size() == center_ranks.size());

  // Initialize per-rank buffers.
  std::vector< std::size_t > center_element_count;
  std::vector< std::size_t > assignments(samples.size());
  std::vector< std::vector< unsigned long > > send_ownership(num_ranks);
  std::vector< unsigned long > send_sizes(num_ranks);
  std::vector< std::pair< int, MPI_Request > > size_send_reqs;
  std::vector< std::pair< int, MPI_Request > > data_send_reqs;
  std::vector< std::vector< unsigned long > > recv_ownership(num_ranks);
  std::vector< unsigned long > recv_sizes(num_ranks);
  std::vector< MPI_Request > size_recv_reqs;
  std::vector< MPI_Request > data_recv_reqs;
  std::vector< MPI_Request > bcast_reqs;

  #ifdef TIMING
    std::vector< unsigned long long > loop_times;
    std::size_t size_size = sizeof(long long);
    std::size_t data_size = sizeof(unsigned long);
    std::vector< std::size_t > send_total, recv_total;
    unsigned long long start_time = rdtsc();
  #endif

  // Run k-means.
  while (true) {
    #ifdef TIMING
      unsigned long long loop_start_time = rdtsc();
    #endif

    int assignments_changed = !!kmeans_assignment< DistT >(rank,
      center_ranks, centers, samples, assignments, send_ownership);

    // Terminate if no assignments have changed globally.
    MPI_Allreduce(MPI_IN_PLACE, &assignments_changed, 1, MPI_INT, MPI_LOR,
      MPI_COMM_WORLD);
    if (!assignments_changed)
      break;

    // Receive the size of each recv buffer from the associated rank.
    for (int i = 0; i < num_ranks; ++i) {
      if (i == rank)
        continue;

      size_recv_reqs.push_back(MPI_Request());
      MPI_Irecv(&recv_sizes[i], 1, MPI_UNSIGNED_LONG, i, 0, MPI_COMM_WORLD,
        &size_recv_reqs.back());
    }

    // Send the size of each send buffer to the associated rank.
    for (int i = 0; i < num_ranks; ++i) {
      if (i == rank)
        continue;

      send_sizes[i] = send_ownership[i].size();
      size_send_reqs.push_back(std::make_pair(i, MPI_Request()));
      MPI_Isend(&send_sizes[i], 1, MPI_UNSIGNED_LONG, i, 0, MPI_COMM_WORLD,
        &size_send_reqs.back().second);
    }

    // Wait for ownership transfers to complete.
    while (!size_recv_reqs.empty() || !data_recv_reqs.empty() ||
      !size_send_reqs.empty() || !data_send_reqs.empty()) {
      int done;
      MPI_Status stat;

      for (std::size_t i = 0; i < size_recv_reqs.size();) {
        MPI_Test(&size_recv_reqs[i], &done, &stat);
        if (!done) {
          ++i;
          continue;
        }

        const int send_rank = stat.MPI_SOURCE;

        recv_ownership[send_rank].resize(recv_sizes[send_rank]);
        if (!recv_ownership[send_rank].empty()) {
          // Request the data for this transfer.
          data_recv_reqs.push_back(MPI_Request());
          MPI_Irecv(&recv_ownership[send_rank][0],
            recv_ownership[send_rank].size(), MPI_UNSIGNED_LONG, send_rank,
            0, MPI_COMM_WORLD, &data_recv_reqs.back());
        }

        // Remove this request.
        std::swap(size_recv_reqs[i], size_recv_reqs.back());
        size_recv_reqs.pop_back();
      }

      for (std::size_t i = 0; i < data_recv_reqs.size();) {
        MPI_Test(&data_recv_reqs[i], &done, &stat);
        if (!done) {
          ++i;
          continue;
        }

        const int send_rank = stat.MPI_SOURCE;

        // Apply the ownership information from this transfer.
        for (std::size_t j = 0; j < recv_ownership[send_rank].size();
          j += 2) {
          assignments[recv_ownership[send_rank][j]] =
            recv_ownership[send_rank][j + 1];
        }
        recv_ownership[send_rank].clear();

        // Remove this request.
        std::swap(data_recv_reqs[i], data_recv_reqs.back());
        data_recv_reqs.pop_back();
      }

      for (std::size_t i = 0; i < size_send_reqs.size();) {
        MPI_Test(&size_send_reqs[i].second, &done, &stat);
        if (!done) {
          ++i;
          continue;
        }

        const int recv_rank = size_send_reqs[i].first;

        if (!send_ownership[recv_rank].empty()) {
          // Send the data for this transfer.
          data_send_reqs.push_back(std::make_pair(recv_rank, MPI_Request()));
          MPI_Isend(&send_ownership[recv_rank][0],
            send_ownership[recv_rank].size(), MPI_UNSIGNED_LONG, recv_rank, 0,
            MPI_COMM_WORLD, &data_send_reqs.back().second);
        }

        // Remove this request.
        std::swap(size_send_reqs[i], size_send_reqs.back());
        size_send_reqs.pop_back();
      }

      for (std::size_t i = 0; i < data_send_reqs.size();) {
        MPI_Test(&data_send_reqs[i].second, &done, &stat);
        if (!done) {
          ++i;
          continue;
        }

        const int recv_rank = data_send_reqs[i].first;

        // Free the send buffer.
        send_ownership[recv_rank].clear();

        // Remove this request.
        std::swap(data_send_reqs[i], data_send_reqs.back());
        data_send_reqs.pop_back();
      }
    }

    // Reset the ownership buffers.
    for (int i = 0; i < num_ranks; ++i) {
      send_ownership[i].clear();
      recv_ownership[i].clear();
    }

    // Calculate the updated cluster centers.
    kmeans_centers(rank, center_ranks, centers, center_element_count, samples,
      assignments);

    // Broadcast the updated cluster centers to all ranks.
    for (std::size_t i = 0; i < centers.size(); ++i) {
      typedef std::is_same< T, float > is_hardcoded_mpi_float_ok;
      assert(is_hardcoded_mpi_float_ok::value);

      if (center_ranks[i] == rank) {
        for (int j = 0; j < num_ranks; ++j) {
          if (j == rank)
            continue;

          bcast_reqs.push_back(MPI_Request());
          MPI_Isend(&centers[i], sizeof(std::array< T, N >) / sizeof(T),
            MPI_FLOAT, j, 0, MPI_COMM_WORLD, &bcast_reqs.back());
        }
      }
      else {
        bcast_reqs.push_back(MPI_Request());
        MPI_Irecv(&centers[i], sizeof(std::array< T, N >) / sizeof(T),
          MPI_FLOAT, center_ranks[i], 0, MPI_COMM_WORLD, &bcast_reqs.back());
      }
    }

    MPI_Waitall(bcast_reqs.size(), &bcast_reqs[0], MPI_STATUSES_IGNORE);
    bcast_reqs.clear();

    #ifdef TIMING
      loop_times.push_back(rdtsc() - loop_start_time);
      std::size_t itr_send_total = 0;
      std::size_t itr_recv_total = 0;
      for (int i = 0; i < send_sizes.size(); ++i) {
        itr_send_total += send_sizes[i];
        itr_recv_total += recv_sizes[i];
      }
      send_total.push_back(itr_send_total*data_size + size_size);
      recv_total.push_back(itr_recv_total*data_size + size_size);
    #endif
  }

  #ifdef TIMING
    if (rank == 0) {
      std::cout << "Rank 0 total time: " << rdtsc()-start_time << "\n";
      std::cout << "Iteration\tTime\tSent\tReceived:\n";
      for (std::size_t i = 0; i < loop_times.size(); ++i) {
        std::cout << i << "\t" << loop_times[i] << "\t"
          << send_total[i] << "\t" << recv_total[i] << "\n";
      }
      std::cout << "\n";
    }
  #endif
}

#endif
